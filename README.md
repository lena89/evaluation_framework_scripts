# Evaluation_Framework_Scripts

This repository contains scripts to the Bachelor Thesis on topic "An Evaluation Framework for Tools to Extract Information from PDF Files". 

There are three folders: text_extraction, tables_extraction and header_metadata_extraction.

--------------------------TEXT------------------------------------------------------------------------------------

Text_extraction contains scripts: missing_paragraphs.py, missing_paragraphs_grobid.py, text_similarity.py

The usage is:

` python3 missing_paragraphs.py -o <outputfolder> -g <groundtruthfolder> `
` python3 missing_paragraphs_grobid.py -o <outputfolder> -g <groundtruthfolder> `
` python3 text_similarity.py -o <outputfolder> -g <groundtruthfolder> `

where outputfolder is a path to the folder of outputs of the tools, and groundtruthfolder is a path to the folder with ground truth .txt files. 

--------------------------HEADER-METADATA-------------------------------------------------------------------------

Header_metadata_extraction contains: header_metadata_xml.py (for tools: cermine, svmheaderparse, mendeley desktop, grobid, parscit and cb2Bib) and header_metadata.py (for such tools as Docsplit, the only parameter needed is pdf folder. To evaluate Docsplit with this script you will need to install Docsplit).

The usage is:

` python3 header_metadata_xml.py -g <gtfolder> -x <xmlfolder> -t <tool> `
` python3 header_metadata.py -p <pdffolder> `

To evaluate Grobid please be sure that you have installed it: https://grobid.readthedocs.io/en/latest/Install-Grobid/
Other libraries needed to be installed to run the scripts are: BeautifulSoup and Levenshtein. 

-------------------------TABLES------------------------------------------------------------------------------------

Tables_extraction contains script: tables_extraction.py. This script works for tables extraction tools with json output (however, one should adjust the script for using it for other tools than tabula, since the structure of output json files can differ).

The usage is:
` python3 tables_extraction.py -x <xmlfolder> -j <jsonfolder> `


Python version, which scripts have been tested with, is 3.5.1. 





