#!/usr/bin/python3

import sys, getopt
import tabula
from tabula import read_pdf
import json
import xml.etree.ElementTree
from lxml import etree
import subprocess
import os
import re
import json
import numpy as np
from scipy.spatial import distance


def euclidean(point1, point2):
    dst = distance.euclidean(point1, point2)
    print(dst)

# GROUND TRUTH
def get_number_of_tables_gt(xml_path):
    root = xml.etree.ElementTree.parse(xml_path)
    tables = []
    for table in root.findall('table'):
        tables.append(table.get('id'))
    return len(tables)


def get_number_of_tables_output(json_file):
    data = json.load(open(json_file))
    return len(data)


def get_cells_gt(xml_path):
    tree = xml.etree.ElementTree.parse(xml_path)
    root = tree.getroot()
    tables = []
    cells = []
    for table in root.iter('region'):
        tables.append(table)
    for i in range(1, len(tables)+1):
        cells.append(len(root.findall('.//table[@id="' + str(i) + '"]/region/cell')))
    return cells


# EXTRACTION
def extract_tabulapdf(pdf_directory, json_directory):
    files_names = os.listdir(pdf_directory)
    for i in range(0, len(files_names)):
        if files_names[i].endswith("pdf"):
            pdf_path = pdf_directory + "/" + files_names[i]
            complete_name = os.path.join(json_directory, os.path.basename(pdf_path) + '.json')
            tabula.convert_into(pdf_path, complete_name, output_format="json")


# extracts all cells from a given json file
# one cells is represented as array with following values:
# x1, x2, y1, y2, text
def extract_json(json_file):
    data = json.load(open(json_file))
    number_of_rows = []
    heights = []
    widths = []
    yone = []
    xone = []
    texts = []
    xtwo = []
    ytwo = []
    cells = []

    for i in range(0, len(data)):
        number_of_rows.append(len(data[i]["data"]))
    for k in range(0, len(number_of_rows)):
        for i in range(0, number_of_rows[k]):
            for element in data[k]["data"][i]:
                heights.append(int(element["height"]))
                widths.append(round(element["width"],0))
                yone.append(round(element["top"],0))
                xone.append(round(element["left"],0))
                texts.append(element["text"])
    for j in range(0, len(heights)):
        xtwo.append(widths[j]+xone[j])
        ytwo.append(heights[j]+yone[j])

    for m in range(0, len(heights)):
        if not (xone[m] == 0.0 and yone[m] == 0.0 and ytwo[m] == 0.0 and xtwo[m] == 0.0):
            cells.append([str(xone[m]), str(yone[m]), str(xtwo[m]), str(ytwo[m]), texts[m]])
    return cells


# extracts cells from a given xml_file
# one cells is represented as array with following values:
# x1, x2, y1, y2, text
def extract_xml(xml_file):
    tree = xml.etree.ElementTree.parse(xml_file)
    root = tree.getroot()
    yone = []
    xone = []
    texts = []
    xtwo = []
    ytwo = []
    cells = []
    for element in tree.iter():
        if element.tag == "bounding-box":
            xone.append(element.get('x1'))
            yone.append(element.get('y1'))
            xtwo.append(element.get('x2'))
            ytwo.append(element.get('y2'))
    for element in tree.iter():
        if element.tag == "content":
            texts.append(element.text)

    for m in range(0, len(yone)):
        if not (xone[m] == 0.0 and yone[m] == 0.0 and ytwo[m] == 0.0 and xtwo[m] == 0.0):
            cells.append([xone[m], yone[m], xtwo[m], ytwo[m], texts[m]])
    return cells


def common_elements(list1, list2):
    return [element for element in list1 if element in list2]


def evaluate_tables_folder(xml_folder, json_folder):
    xml_files = sorted(os.listdir(xml_folder))
    json_files = sorted(os.listdir(json_folder))
    cells_scores = []
    tables_scores = []
    for i in range(0, len(xml_files)):
        xml_file = xml_folder + "/" + xml_files[i]
        json_file = json_folder + "/" + json_files[i]
        cells_scores.append(correct_cells(xml_file, json_file))
        tables_scores.append(detected_tables(xml_file, json_file))
    average_cells = sum(cells_scores)/len(cells_scores)
    average_tables = sum(tables_scores)/len(tables_scores)
    print("Average percent of detected tables is " + str(average_tables))
    print("Average percent of corrected cells is " + str(average_cells))


def detected_tables(xml_file, json_file):
    score_for_tables = get_number_of_tables_output(json_file) / get_number_of_tables_gt(xml_file)
    return score_for_tables


def correct_cells(xml_file, json_file):
    cells_gt = extract_xml(xml_file)
    #print(cells_gt)
    #print(len(cells_gt))
    #number_of_cells_gt = 0
    cells_output = extract_json(json_file)
    #print(cells_output)
    cells_gt_array = get_cells_gt(xml_file)
    #for i in range(0, get_number_of_tables_output(json_file)):
    #    number_of_cells_gt += cells_gt_array[i]
    textsgt = []
    textsoutput = []
    #output_number_json = re.sub("eu-001-|\.json", "", (os.path.basename(json_file)))
    #print(output_number_json)
    #print(cells_gt_array)
    #number_of_cells_gt = cells_gt_array[int(output_number_json)-1]
    #indexone = sum(cells_gt_array[0:int(output_number_json)-1])-1
    #indextwo = indexone + cells_gt_array[int(output_number_json)-1]

    for i in range(0, len(cells_gt)):
        textsgt.append(cells_gt[i][4]) # 4, because text stays on place 4! ['340', '619', '371', '629', 'Species']
    for i in range(0, len(cells_output)):
        textsoutput.append(cells_output[i][4])
    output_cells = len(common_elements(textsgt, textsoutput))
    cor_cells = output_cells/len(cells_gt)
    #for i in range(0, number_of_cells_gt):
    #    for j in range(0, len(textstwo)):
    #        if(textsone[i] == textstwo[j]):
    #            print("ja")
    #            #print(str(i)+str(textsone[i])+str(textstwo[j]))
    #            if(str(i)+str(textsone[i])+str(textstwo[j]) not in scores):
    #                scores.append(str(i)+str(textsone[i])+str(textstwo[j]))
    #print("Number of detected tables is " + str(score_for_tables))
    #print("Number of correctly extracted cells is " + str(cor_cells))
    return cor_cells


def main(argv):
    xmlfolder = ''
    jsonfolder = ''
    try:
        opts, args = getopt.getopt(argv, "hx:j:", ["xmlfolder=", "jsonfolder="])
    except getopt.GetoptError:
        print('tables_extraction.py -x <xmlfolder> -j <jsonfolder>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('tables_extraction.py -x <xmlfolder> -j <jsonfolder>')
            sys.exit()
        elif opt in ("-x", "--xmlfolder"):
            xmlfolder = arg
        elif opt in ("-j", "--jsonfolder"):
            jsonfolder = arg
    evaluate_tables_folder(xmlfolder, jsonfolder)


if __name__ == "__main__":
    main(sys.argv[1:])