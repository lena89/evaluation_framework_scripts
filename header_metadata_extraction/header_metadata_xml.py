#!/usr/bin/python3

import sys, getopt
import urllib3
import os
from bs4 import BeautifulSoup
import re
import csv
from Levenshtein import distance
from lxml import objectify


def cermine_title(xml_path):
    with open(xml_path, 'rb') as file:
        xml = file.read()
    soup = BeautifulSoup(xml, 'html.parser')
    titles = []
    for tag in soup.findAll('article-title'):
        titles.append(tag.text)
    if len(titles) == 0:
        main_title = ""
    else:
        main_title = titles[0]
    return main_title


def cermine_author(xml_path):
    with open(xml_path, 'rb') as file:
        xml = file.read()
    names = []
    soup = BeautifulSoup(xml, 'html.parser')
    for tag in soup.findAll("contrib"):
        names.append([str(tag.forename.text).lower().replace(" ", "").replace(".", ""), str(tag.surname.text).lower()])
    return names


def cermine_abstract(xml_path):
    with open(xml_path, 'rb') as file:
        xml = file.read()
    soup = BeautifulSoup(xml, 'html.parser')
    abstracts = []
    for tag in soup.findAll('abstract'):
        abstracts.append(re.sub('\n', "", tag.text))
    if len(abstracts) == 0:
        abstract = ""
    else:
        abstract = abstracts[0]
    return abstract


def cermine_year(xml_path):
    with open(xml_path, 'rb') as file:
        xml = file.read()
    soup = BeautifulSoup(xml, 'html.parser')
    years = []
    for tag in soup.findAll('year'):
        years.append(tag.text)
    if len(years) == 0:
        year = ""
    else:
        year = years[0]
    return year


def cermine_affiliation(xml_path):
    with open(xml_path, 'rb') as file:
        xml = file.read()
    soup = BeautifulSoup(xml, 'html.parser')
    affiliations = []
    for tag in soup.findAll('institution'):
        affiliations.append(str(tag.text))
    if len(affiliations) == 0:
        affiliation = ""
    else:
        affiliation = " ".join(affiliations)
    return affiliation


def cermine_source(xml_path):
    with open(xml_path, 'rb') as file:
        xml = file.read()
    soup = BeautifulSoup(xml, 'html.parser')
    journals = []
    for tag in soup.findAll('journal-title'):
        journals.append(str(tag.text))
    if len(journals) == 0:
        journal = ""
    else:
        journal = " ".join(journals)
    return journal


def xml_author(xml_path):
    with open(xml_path, 'rb') as file:
        xml = file.read()
    names = []
    soup = BeautifulSoup(xml, 'html.parser')
    for tag in soup.findAll("author"):
        names.append([str(tag.forename.text).lower().replace(" ", "").replace(".", ""), str(tag.surname.text).lower()])
    return names


def xml_title(xml_path):
    with open(xml_path, 'rb') as file:
        xml = file.read()
    soup = BeautifulSoup(xml, 'html.parser')
    title = soup.find('title').text
    return title


def xml_year(xml_path):
    with open(xml_path, 'rb') as file:
        xml = file.read()
    soup = BeautifulSoup(xml, 'html.parser')
    year = soup.find('year').text
    return year


def xml_affiliation(xml_path):
    with open(xml_path, 'rb') as file:
        xml = file.read()
    soup = BeautifulSoup(xml, 'html.parser')
    affiliation = soup.find('affiliation').text
    return affiliation


def xml_journal(xml_path):
    with open(xml_path, 'rb') as file:
        xml = file.read()
    soup = BeautifulSoup(xml, 'html.parser')
    journal = soup.find('journal').text
    return journal


def xml_abstract(xml_path):
    with open(xml_path, 'rb') as file:
        xml = file.read()
    soup = BeautifulSoup(xml, 'html.parser')
    abstract = soup.find('abstract').text
    return abstract


# GROBID
def grobid_title(xml_path):
    with open(xml_path, 'rb') as file:
        xml = file.read()
    root = objectify.fromstring(xml)
    title = root.teiHeader.fileDesc.titleStmt.title
    return str(title)


# this function returns list of the lists of authors, each sublist contains forename and surname
def grobid_authors(xml_path):
    with open(xml_path, 'rb') as file:
        xml = file.read()
    names = []
    root = objectify.fromstring(xml)
    authors_list = root.teiHeader.fileDesc.sourceDesc.biblStruct.analytic.author
    for i in range(0, len(authors_list)):
        for j in authors_list[i].persName:
            names.append([str(j.forename).lower().replace(" ", ""), str(j.surname).lower()])
    return names


def grobid_affiliations(xml_path):
    with open(xml_path, 'rb') as file:
        xml = file.read()
    root = objectify.fromstring(xml)
    authors_list = root.teiHeader.fileDesc.sourceDesc.biblStruct.analytic.author
    children_of_authors = []
    for i in authors_list:
        children_of_authors.append(str(i.getchildren()))
    affiliations = []
    affiliations_final = []
    for i in range(0, len(authors_list)):
        if "affiliation" in children_of_authors[i]:
            for j in authors_list[i].affiliation:
                for k in j.orgName:
                    affiliations.append(k)
                for l in j.address:
                    address = l.getchildren()
                    address_str = map(str, address)
                    affiliations.append(' '.join(address_str))

    for i in range(0, len(affiliations)):
        if affiliations[i] not in affiliations_final:
            affiliations_final.append(str(affiliations[i]))
    return str(' '.join(affiliations_final)).lower()


def grobid_abstract(xml_path):
    with open(xml_path, 'rb') as file:
        xml = file.read()
    root = objectify.fromstring(xml)
    try:
        abstract_list = root.teiHeader.profileDesc.abstract.p
        abstract = ""
        for p in abstract_list:
            abstract += p + '\n'
    except AttributeError as error:
        abstract = ""
    return str(abstract)


def grobid_year(xml_path):
    with open(xml_path, 'rb') as file:
        xml = file.read()
    root = objectify.fromstring(xml)
    try:
        year = root.teiHeader.fileDesc.publicationStmt.date
    except AttributeError as error:
        year = "no year"
    return str(year)


# EVALUATION
def convert_to_string(doc_path):
    with open(doc_path, 'r') as myfile:
        data = myfile.read().replace('\n', ' ')
    return data


def score(gt, output):
    score = distance(gt, output)
    return score


def score_lev_reversed(gt, output):
    score = (len(gt) - distance(gt, output)) / len(gt) * 100
    if score < 0:
        score = 0
    return score


def score_year(string1, string2):
    if string1 == string2:
        score_ye = 1
        return score_ye
    else:
        score_ye = 0
        return score_ye


def total_score_mendeley_cb2bib(gt_folder, output_folder):
    gt_files = sorted(os.listdir(gt_folder))
    output_files = sorted(os.listdir(output_folder))
    title_gts = []
    author_gts = []
    year_gts = []
    abstract_gts = []
    journal_gts = []

    title_outputs = []
    author_outputs = []
    year_outputs = []
    abstract_outputs = []
    journal_outputs = []

    title_scores = []
    author_scores = []
    year_scores = []
    abstract_scores = []
    journal_scores = []

    for i in range(0, len(gt_files)):
        gt_path = gt_folder + "/" + gt_files[i]
        output_path = output_folder + "/" + output_files[i]

        title_gts.append(xml_title(gt_path))
        title_outputs.append(xml_title(output_path))

        author_gts.append(xml_author(gt_path))
        author_outputs.append(xml_author(output_path))

        year_gts.append(xml_year(gt_path))
        year_outputs.append(xml_year(output_path))

        abstract_gts.append(xml_abstract(gt_path))
        abstract_outputs.append(xml_abstract(output_path))

        journal_gts.append(xml_journal(gt_path))
        journal_outputs.append(xml_journal(output_path))

    for i in range(0, len(gt_files)):
        if (title_gts[i]):
            title_scores.append(score_lev_reversed(title_gts[i], title_outputs[i]))
        else:
            print("gt is empty")
        if (author_gts[i]):
            author_scores.append(score_authors_xml(gt_folder + "/" + gt_files[i], output_folder + "/" + output_files[i]))
        else:
            print("gt is empty")
        if year_gts[i]:
            year_scores.append(score_year(year_gts[i], year_outputs[i]))
        else:
            print("gt is empty")
        if abstract_gts[i]:
            abstract_scores.append(score_lev_reversed(abstract_gts[i], abstract_outputs[i]))
        if journal_gts[i]:
            journal_scores.append(score_lev_reversed(journal_gts[i], journal_outputs[i]))

    average_title = sum(title_scores) / len(title_scores)
    print("average score for title " + str(average_title))
    average_authors = sum(author_scores) / len(author_scores)
    print("average score for authors " + str(average_authors))
    average_year = sum(year_scores) / len(year_scores)
    print("averge score for year " + str(average_year))
    average_abstract = sum(abstract_scores) / len(abstract_scores)
    print("average score for abstract " + str(average_abstract))
    average_journal = sum(journal_scores) / len(journal_scores)
    print("average score for journal " + str(average_journal))


# svm: title, year, abstract, affiliation, author
def total_score_svm(gt_folder, output_folder):
    gt_files = sorted(os.listdir(gt_folder))
    output_files = sorted(os.listdir(output_folder))
    title_gts = []
    author_gts = []
    year_gts = []
    abstract_gts = []
    affiliation_gts = []

    title_outputs = []
    author_outputs = []
    year_outputs = []
    abstract_outputs = []
    affiliation_outputs = []

    title_scores = []
    author_scores = []
    year_scores = []
    abstract_scores = []
    affiliation_scores = []

    for i in range(0, len(gt_files)):
        gt_path = gt_folder + "/" + gt_files[i]
        output_path = output_folder + "/" + output_files[i]

        title_gts.append(xml_title(gt_path))
        title_outputs.append(xml_title(output_path))

        author_gts.append(xml_author(gt_path))
        author_outputs.append(xml_author(output_path))

        year_gts.append(xml_year(gt_path))
        year_outputs.append(xml_year(output_path))

        abstract_gts.append(xml_abstract(gt_path))
        abstract_outputs.append(xml_abstract(output_path))

        affiliation_gts.append(xml_affiliation(gt_path))
        affiliation_outputs.append(xml_affiliation(output_path))

    for i in range(0, len(gt_files)):
        if (title_gts[i]):
            title_scores.append(score_lev_reversed(title_gts[i], title_outputs[i]))
        else:
            print("gt is empty")
        if (author_gts[i]):
            author_scores.append(
                score_authors_xml(gt_folder + "/" + gt_files[i], output_folder + "/" + output_files[i]))
        else:
            print("gt is empty")
        if year_gts[i]:
            year_scores.append(score_year(year_gts[i], year_outputs[i]))
        else:
            print("gt is empty")
        if abstract_gts[i]:
            abstract_scores.append(score_lev_reversed(abstract_gts[i], abstract_outputs[i]))
        if affiliation_gts[i]:
            affiliation_scores.append(score_lev_reversed(affiliation_gts[i], affiliation_outputs[i]))

    average_title = sum(title_scores) / len(title_scores)
    print("average score for title " + str(average_title))
    average_authors = sum(author_scores) / len(author_scores)
    print("average score for authors " + str(average_authors))
    average_year = sum(year_scores) / len(year_scores)
    print("averge score for year " + str(average_year))
    average_abstract = sum(abstract_scores) / len(abstract_scores)
    print("average score for abstract " + str(average_abstract))
    average_affiliation = sum(affiliation_scores) / len(affiliation_scores)
    print("average score for affiliation " + str(average_affiliation))


def total_score_parscit(gt_folder, output_folder):
    gt_files = sorted(os.listdir(gt_folder))
    output_files = sorted(os.listdir(output_folder))
    title_gts = []
    author_gts = []
    abstract_gts = []
    affiliation_gts = []

    title_outputs = []
    author_outputs = []
    abstract_outputs = []
    affiliation_outputs = []

    title_scores = []
    author_scores = []
    abstract_scores = []
    affiliation_scores = []

    for i in range(0, len(gt_files)):
        gt_path = gt_folder + "/" + gt_files[i]
        output_path = output_folder + "/" + output_files[i]

        title_gts.append(xml_title(gt_path))
        title_outputs.append(xml_title(output_path))

        author_gts.append(xml_author(gt_path))
        author_outputs.append(xml_author(output_path))

        abstract_gts.append(xml_abstract(gt_path))
        abstract_outputs.append(xml_abstract(output_path))

        affiliation_gts.append(xml_affiliation(gt_path))
        affiliation_outputs.append(xml_affiliation(output_path))

    for i in range(0, len(gt_files)):
        if (title_gts[i]):
            title_scores.append(score_lev_reversed(title_gts[i], title_outputs[i]))
        else:
            print("gt is empty")
        if (author_gts[i]):
            author_scores.append(
                score_authors_xml(gt_folder + "/" + gt_files[i], output_folder + "/" + output_files[i]))
        else:
            print("gt is empty")
        if abstract_gts[i]:
            abstract_scores.append(score_lev_reversed(abstract_gts[i], abstract_outputs[i]))
        if affiliation_gts[i]:
            affiliation_scores.append(score_lev_reversed(affiliation_gts[i], affiliation_outputs[i]))

    average_title = sum(title_scores) / len(title_scores)
    print("average score for title " + str(average_title))
    average_authors = sum(author_scores) / len(author_scores)
    print("average score for authors " + str(average_authors))
    average_abstract = sum(abstract_scores) / len(abstract_scores)
    print("average score for abstract " + str(average_abstract))
    average_affiliation = sum(affiliation_scores) / len(affiliation_scores)
    print("average score for affiliation " + str(average_affiliation))


def score_authors_xml(gt_file, output_file):
    gt_authors = xml_author(gt_file)
    output_authors = xml_author(output_file)
    scores = []
    for i in range(0, len(gt_authors)):
        score_forename = score_lev_reversed(gt_authors[i][0], output_authors[i][0])
        if score_forename < 0:
            score_forename = 0
        score_surname = score_lev_reversed(gt_authors[i][1], output_authors[i][1])
        if score_surname < 0:
            score_surname = 0
        score = (score_forename + score_surname) / 2
        scores.append(score)
    average = sum(scores) / len(scores)
    return average


def score_authors_grobid(gt_file, output_file):
    gt_authors = xml_author(gt_file)
    output_authors = grobid_authors(output_file)
    scores = []
    for i in range(0, len(gt_authors)):
        score_forename = score_lev_reversed(gt_authors[i][0], output_authors[i][0])
        if score_forename < 0:
            score_forename = 0
        score_surname = score_lev_reversed(gt_authors[i][1], output_authors[i][1])
        if score_surname < 0:
            score_surname = 0
        score = (score_forename + score_surname) / 2
        scores.append(score)
    average = sum(scores) / len(scores)
    return average


def score_authors_cermine(gt_file, output_file):
    gt_authors = xml_author(gt_file)
    output_authors = cermine_author(output_file)
    scores = []
    for i in range(0, len(gt_authors)):
        score_forename = score_lev_reversed(gt_authors[i][0], output_authors[i][0])
        if score_forename < 0:
            score_forename = 0
        score_surname = score_lev_reversed(gt_authors[i][1], output_authors[i][1])
        if score_surname < 0:
            score_surname = 0
        score = (score_forename + score_surname) / 2
        scores.append(score)
    average = sum(scores) / len(scores)
    return average


# CERMINE
def total_score_cermine(gt_folder, output_folder):
    gt_files = sorted(os.listdir(gt_folder))
    output_files = sorted(os.listdir(output_folder))
    xml_files = []
    for i in range(0, len(output_files)):
        if output_files[i].endswith("cermxml"):
            xml_files.append(output_files[i])
    title_gts = []
    authors_gts = []
    years_gts = []
    affiliations_gts = []
    journal_gts = []
    abstract_gts = []

    title_outputs = []
    authors_outputs = []
    years_outputs = []
    affiliations_outputs = []
    journal_outputs = []
    abstract_outputs = []

    title_scores = []
    authors_scores = []
    years_scores = []
    affiliations_scores = []
    journal_scores = []
    abstract_scores = []

    for i in range(0, len(gt_files)):
        gt_path = gt_folder + "/" + gt_files[i]
        title_gts.append(xml_title(gt_path))
        authors_gts.append(xml_author(gt_path))
        years_gts.append(xml_year(gt_path))
        affiliations_gts.append(xml_affiliation(gt_path))
        journal_gts.append(xml_journal(gt_path))
        abstract_gts.append(xml_abstract(gt_path))
    for i in range(0, len(output_files)):
        output_path = output_folder + "/" + output_files[i]
        title_outputs.append(cermine_title(output_path))
        authors_outputs.append(cermine_author(output_path))
        years_outputs.append(cermine_year(output_path))
        affiliations_outputs.append(cermine_affiliation(output_path))
        journal_outputs.append(cermine_source(output_path))
        abstract_outputs.append(cermine_abstract(output_path))
    if len(gt_files) == len(xml_files):
        for i in range(0, len(gt_files)):
            if not (len(title_gts[i]) == 0):
                title_scores.append(score_lev_reversed(str(title_gts[i]), str(title_outputs[i])))
            else:
                print("no groundtruth")
            if not (len(authors_gts[i]) == 0):
                authors_scores.append(
                    score_authors_cermine(gt_folder + "/" + gt_files[i], output_folder + "/" + output_files[i]))
            else:
                print("no groundtruth")
            if not (len(affiliations_gts[i]) == 0):
                affiliations_scores.append(score_lev_reversed(str(affiliations_gts[i]), str(affiliations_outputs[i])))
            else:
                print("no groundtruth")
            if not (len(journal_gts[i]) == 0):
                journal_scores.append(score_lev_reversed(str(journal_gts[i]), str(journal_outputs[i])))
            else:
                print("no groundtruth")
            if not (len(abstract_gts[i]) == 0):
                abstract_scores.append(score_lev_reversed(str(abstract_gts[i]), str(abstract_outputs[i])))
            else:
                print("no groundtruth")
            if not (len(years_gts[i]) == 0):
                years_scores.append(score_year(str(years_gts[i]), str(years_outputs[i])))
            else:
                print("no groundtruth")
        average_title = sum(title_scores) / len(title_scores)
        average_author = sum(authors_scores) / len(authors_scores)
        average_year = sum(years_scores) / len(years_scores)
        average_affiliation = sum(affiliations_scores) / len(affiliations_scores)
        average_journal = sum(journal_scores) / len(journal_scores)
        average_abstract = sum(abstract_scores) / len(abstract_scores)
        print("Average score for title is " + str(average_title))
        print("Average score for authors is " + str(average_author))
        print("Average score for year is " + str(average_year))
        print("Average score for affiliation is " + str(average_affiliation))
        print("Average score for journal is " + str(average_journal))
        print("Average score for abstract is " + str(average_abstract))
    else:
        print("Length of folders is different!")


def total_score_grobid(gt_folder, output_folder):
    gt_files = sorted(os.listdir(gt_folder))
    output_files = sorted(os.listdir(output_folder))
    xml_files = []
    for i in range(0, len(output_files)):
        if output_files[i].endswith(".tei.xml"):
            xml_files.append(output_files[i])
    title_gts = []
    authors_gts = []
    years_gts = []
    affiliations_gts = []
    abstract_gts = []

    title_outputs = []
    authors_outputs = []
    years_outputs = []
    affiliations_outputs = []
    abstract_outputs = []

    title_scores = []
    authors_scores = []
    years_scores = []
    affiliations_scores = []
    abstract_scores = []

    for i in range(0, len(gt_files)):
        gt_path = gt_folder + "/" + gt_files[i]
        title_gts.append(xml_title(gt_path))
        authors_gts.append(xml_author(gt_path))
        years_gts.append(xml_year(gt_path))
        affiliations_gts.append(xml_affiliation(gt_path))
        abstract_gts.append(xml_abstract(gt_path))
    for i in range(0, len(output_files)):
        output_path = output_folder + "/" + output_files[i]
        title_outputs.append(grobid_title(output_path))
        authors_outputs.append(cermine_author(output_path))
        years_outputs.append(grobid_year(output_path))
        affiliations_outputs.append(grobid_affiliations(output_path))
        abstract_outputs.append(grobid_abstract(output_path))
    if len(gt_files) == len(xml_files):
        for i in range(0, len(gt_files)):
            if not (len(title_gts[i]) == 0):
                title_scores.append(score_lev_reversed(str(title_gts[i]), str(title_outputs[i])))
            else:
                print("no groundtruth")
            if not (len(authors_gts[i]) == 0):
                authors_scores.append(
                    score_authors_cermine(gt_folder + "/" + gt_files[i], output_folder + "/" + output_files[i]))
            else:
                print("no groundtruth")
            if not (len(affiliations_gts[i]) == 0):
                affiliations_scores.append(score_lev_reversed(str(affiliations_gts[i]), str(affiliations_outputs[i])))
            else:
                print("no groundtruth")
            if not (len(abstract_gts[i]) == 0):
                abstract_scores.append(score_lev_reversed(str(abstract_gts[i]), str(abstract_outputs[i])))
            else:
                print("no groundtruth")
            if not (len(years_gts[i]) == 0):
                years_scores.append(score_year(str(years_gts[i]), str(years_outputs[i])))
            else:
                print("no groundtruth")
        average_title = sum(title_scores) / len(title_scores)
        average_author = sum(authors_scores) / len(authors_scores)
        average_year = sum(years_scores) / len(years_scores)
        average_affiliation = sum(affiliations_scores) / len(affiliations_scores)
        average_abstract = sum(abstract_scores) / len(abstract_scores)
        print("Average score for title is " + str(average_title))
        print("Average score for authors is " + str(average_author))
        print("Average score for year is " + str(average_year))
        print("Average score for affiliation is " + str(average_affiliation))
        print("Average score for abstract is " + str(average_abstract))
    else:
        print("Length of folders is different!")


def main(argv):
    gtfolder = ''
    xmlfolder = ''
    tool = ''
    try:
        opts, args = getopt.getopt(argv, "hg:x:t:", ["gtfolder=", "xmlfolder=", "tool="])
    except getopt.GetoptError:
        print('header_metadata_xml.py -g <gtfolder> -x <xmlfolder> -t <tool>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('header_metadata_xml.py -g <gtfolder> -x <xmlfolder> -t <tool>')
            sys.exit()
        elif opt in ("-g", "--gtfolder"):
            pdffolder = arg
        elif opt in ("-x", "--xmlfolder"):
            xmlfolder = arg
        elif opt in ("-t", "--tool"):
            tool = arg
    if tool == "cermine":
        total_score_cermine(gtfolder, xmlfolder)
    elif tool == "grobid":
        total_score_grobid(gtfolder, xmlfolder)
    elif tool == "svm":
        total_score_svm(gtfolder, xmlfolder)
    elif tool == "parscit":
        total_score_parscit(gtfolder, xmlfolder)
    elif tool == "mendeley":
        total_score_mendeley_cb2bib(gtfolder, xmlfolder)
    elif tool == "cb2bib":
        total_score_mendeley_cb2bib(gtfolder, xmlfolder)
    else:
        print(
            "This tool cannot be evaluated with this script. Available tools are: cermine, grobid, mendeley, cb2bib, parscit, svmheaderparse")


if __name__ == "__main__":
    main(sys.argv[1:])
