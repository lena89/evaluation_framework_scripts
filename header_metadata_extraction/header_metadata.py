#!/usr/bin/python3

import sys, getopt
import urllib3
import os
from bs4 import BeautifulSoup
import re
from Levenshtein import distance
import subprocess


# GROUND TRUTH GENERATION
def get_ids(path):
    files_names = sorted(os.listdir(path))
    files_names_without_ext = []
    arxiv_ids = []
    for i in range(0, len(files_names)):
        files_names_without_ext.append(os.path.splitext(files_names[i])[0])
    for i in range(0, len(files_names_without_ext)):
        index = files_names_without_ext[i].find("0")
        arxiv_ids.append(files_names_without_ext[i][0:index] + "/" + files_names_without_ext[i][index:len(files_names_without_ext[i])])
    return arxiv_ids


def export_title(art_id):
    http = urllib3.PoolManager()
    query = "http://export.arxiv.org/api/query?id_list=" + art_id
    response = http.request('GET', query)
    html = response.data
    soup = BeautifulSoup(html, 'html.parser')
    title = soup.find_all("title")[1].text
    title_new = re.sub('\n', ' ', title)
    return re.sub(' +', ' ', title_new)


def export_authors(art_id):
    http = urllib3.PoolManager()
    query = "http://export.arxiv.org/api/query?id_list=" + art_id
    response = http.request('GET', query)
    html = response.data
    soup = BeautifulSoup(html, 'html.parser')
    authors = soup.find_all("name")
    authors_text = []
    for author in authors:
        authors_text.append(re.sub('\n', '', author.text))
    authors_final = ' '.join(authors_text)
    return authors_final.lower()


def export_year(art_id):
    http = urllib3.PoolManager()
    query = "http://export.arxiv.org/api/query?id_list=" + art_id
    response = http.request('GET', query)
    html = response.data
    soup = BeautifulSoup(html, 'html.parser')
    published = soup.find_all("published")[0].text
    year = published[0:4]
    return year


# EXTRACTION
def docsplit_title(pdf_path):
    process = subprocess.Popen(
        'docsplit title ' + pdf_path,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        shell=True)
    process_out, process_err = process.communicate()
    if process_out == "b''":
        title = ""
    else:
        title = re.sub("b''", "", str(process_out))
    return title


def docsplit_author(pdf_path):
    process = subprocess.Popen(
        'docsplit author ' + pdf_path,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        shell=True)
    process_out, process_err = process.communicate()
    if process_out == "b''":
        author = ""
    else:
        author = re.sub("b''", "", str(process_out))
    return author.lower()


def docsplit_year(pdf_path):
    process = subprocess.Popen(
        'docsplit date ' + pdf_path,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        shell=True)
    process_out, process_err = process.communicate()
    year = str(process_out)[-7:]
    year_new = year[0:4]
    return str(year_new)


# EVALUATION
def convert_to_string(doc_path):
    with open(doc_path, 'r') as myfile:
        data = myfile.read().replace('\n', ' ')
    return data


def score(gt, output):
    score = distance(gt, output)
    return score


def score_lev_reversed(gt, output):
    score = (len(gt) - distance(gt, output)) / len(gt) * 100
    return score


def score_year(string1, string2):
    if string1 == string2:
        score_ye = 1
        return score_ye
    else:
        score_ye = 0
        return score_ye


def total_score_title_docsplit(pdf_folder):
    doc_ids = get_ids(pdf_folder)
    pdf_files = sorted(os.listdir(pdf_folder))
    title_gts = []
    title_outputs = []
    scores = []
    for i in range(0, len(doc_ids)):
        title_gts.append(str(export_title(doc_ids[i])))
    for i in range(0, len(pdf_files)):
        title_outputs.append(str(docsplit_title(pdf_folder + "/" + pdf_files[i])))
    for i in range(0, len(title_gts)):
        if not(len(title_gts[i]) == 0):
            scores.append(score_lev_reversed(title_gts[i], title_outputs[i]))
    scores_new = []
    for i in range(0, len(scores)):
        if scores[i] < 0:
            scores_new.append(0)
        else:
            scores_new.append(scores[i])
    if len(scores) == 0:
        print("No scores!")
        return 0
    else:
        average = sum(scores_new) / len(scores_new)
        print("Average for title is " + str(average))
        return average


def total_score_author_docsplit(pdf_folder):
    doc_ids = get_ids(pdf_folder)
    pdf_files = sorted(os.listdir(pdf_folder))
    author_gts = []
    author_outputs = []
    scores = []
    for i in range(0, len(doc_ids)):
        author_gts.append(str(export_authors(doc_ids[i])))
    for i in range(0, len(pdf_files)):
        author_outputs.append(str(docsplit_author(pdf_folder + "/" + pdf_files[i])))
    for i in range(0, len(author_gts)):
        if not(len(author_gts[i]) == 0):
            scores.append(score_lev_reversed(author_gts[i], author_outputs[i]))
    scores_new = []
    for i in range(0, len(scores)):
        if scores[i] < 0:
            scores_new.append(0)
        else:
            scores_new.append(scores[i])
    if len(scores) == 0:
        print("No scores!")
        return 0
    else:
        average = sum(scores_new) / len(scores_new)
        print("Average for author is " + str(average))
        return average


def total_score_year_docsplit(pdf_folder):
    doc_ids = get_ids(pdf_folder)
    pdf_files = sorted(os.listdir(pdf_folder))
    year_gts = []
    year_outputs = []
    scores = []
    for i in range(0, len(doc_ids)):
        year_gts.append(str(export_year(doc_ids[i])))
    for i in range(0, len(pdf_files)):
        year_outputs.append(str(docsplit_year(pdf_folder + "/" + pdf_files[i])))
    for i in range(0, len(year_gts)):
        scores.append(score_year(year_gts[i], year_outputs[i]))
    if len(scores) == 0:
        total_score = 0
    else:
        total_score = sum(scores)/len(year_gts)
    print("Average for year is " + str(total_score))
    return total_score


def main(argv):
    pdffolder = ''
    try:
        opts, args = getopt.getopt(argv, "hp:", ["pdffolder="])
    except getopt.GetoptError:
        print('header_metadata.py -p <pdffolder>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('header_metadata.py -p <pdffolder>')
            sys.exit()
        elif opt in ("-p", "--pdffolder"):
            pdffolder = arg
    total_score_author_docsplit(pdffolder)
    total_score_title_docsplit(pdffolder)
    total_score_year_docsplit(pdffolder)


if __name__ == "__main__":
    main(sys.argv[1:])
