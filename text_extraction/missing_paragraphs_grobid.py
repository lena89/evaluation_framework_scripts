#!/usr/bin/python3

import sys, getopt
import os
import re
from bs4 import BeautifulSoup


def size_of_file(file):
    contents = convert_to_string_with_paras(file)
    return len(contents)


def convert_to_string_with_paras(doc_path):
    with open(doc_path, 'r') as myfile:
        data = myfile.read()
    return data


def convert_to_string(doc_path):
    with open(doc_path, 'r') as myfile:
        data = myfile.read().replace('\n', ' ')
    return data


def paras_of_text(doc_path):
    data = convert_to_string_with_paras(doc_path)
    paras = data.split('\n\n')
    paras[:] = (value for value in paras if value != '\t')
    return len(paras)


def paras_of_text_output(doc_path):
    data = convert_to_string_with_paras(doc_path)
    paras = data.split('\n')
    paras[:] = (value for value in paras if value != '\t')
    return len(paras)


def number_of_missing_paras_grobid(xml_path, gt_file):
    gt_paras = paras_of_text(gt_file)
    output_paras = grobid_number_of_paras(xml_path)
    missing_paras = 0
    if gt_paras >= output_paras:
        missing_paras = gt_paras - output_paras
    score = float(missing_paras) / float(gt_paras)
    return score


# number of paragraphs for grobid
def grobid_number_of_paras(xml_path):
    with open(xml_path, 'rb') as file:
        xml = file.read()
    soup = BeautifulSoup(xml, 'html.parser')
    p = 0
    formula = 0
    head = 0
    biblstruct = 0
    for tag in soup.findAll('p'):
        p += 1
    for tag in soup.findAll('formula'):
        formula += 1
    for tag in soup.findAll('biblstruct'):
        biblstruct += 1
    for tag in soup.findAll(''):
        head += 1
    paras = p + formula + head + biblstruct
    return paras


def number_of_missing_paras(output_file, gt_file):
    gt_paras = paras_of_text(gt_file)
    output_paras = paras_of_text_output(output_file)
    missing_paras = 0
    if gt_paras >= output_paras:
        missing_paras = gt_paras - output_paras
    return float(missing_paras) / float(gt_paras)


def evaluate_mis_paras_grobid(xml_dir, gt_dir):
    xml_files = sorted(os.listdir(xml_dir))
    gt_files = sorted(os.listdir(gt_dir))
    xml_files_without_ext = []
    gt_files_without_ext = []
    scores = []
    for i in range(0, len(xml_files)):
        xml_files_without_ext.append(re.sub("\.xml", "", xml_files[i]))
    for i in range(0, len(gt_files)):
        gt_files_without_ext.append(re.sub("\.txt", "", gt_files[i]))
    for i in range(0, len(xml_files)):
        gt_path = gt_dir + "/" + gt_files[i]
        xml_path = xml_dir + "/" + xml_files[i]
        if xml_files_without_ext[i] == gt_files_without_ext[i]:
            scores.append(number_of_missing_paras_grobid(xml_path, gt_path))
    print(scores)
    average = sum(scores)/len(scores)
    if len(scores) == 0:
        print("Length of scores is 0")
    else:
        print("Average is " + str(average))


#use xml output of grobid
def main(argv):
    outputfolder = ''
    groundtruthfolder = ''
    try:
        opts, args = getopt.getopt(argv, "ho:g:", ["outputfolder=", "groundtruthfolder="])
    except getopt.GetoptError:
        print('missing_paragraphs_grobid.py -o <outputfolder> -g <groundtruthfolder>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('missing_paragraphs_grobid.py -o <outputfolder> -g <groundtruthfolder>')
            sys.exit()
        elif opt in ("-o", "--outputfolder"):
            outputfolder = arg
        elif opt in ("-g", "--groundtruthfolder"):
            groundtruthfolder = arg
    evaluate_mis_paras_grobid(outputfolder, groundtruthfolder)


if __name__ == "__main__":
    main(sys.argv[1:])
