#!/usr/bin/python3

import sys, getopt
import os


def size_of_file(file):
    contents = convert_to_string_with_paras(file)
    return len(contents)


def convert_to_string_with_paras(doc_path):
    with open(doc_path, 'r') as myfile:
        data = myfile.read()
    return data


def convert_to_string(doc_path):
    with open(doc_path, 'r') as myfile:
        data = myfile.read().replace('\n', ' ')
    return data


def paras_of_text(doc_path):
    data = convert_to_string_with_paras(doc_path)
    paras = data.split('\n\n')
    paras[:] = (value for value in paras if value != '\t')
    return len(paras)


def number_of_missing_paras(output_file, gt_file):
    gt_paras = paras_of_text(gt_file)
    output_paras = paras_of_text(output_file)
    missing_paras = 0
    if gt_paras >= output_paras:
        missing_paras = gt_paras - output_paras
    return float(missing_paras) / float(gt_paras)


def evaluate_paras(output_dir, gt_dir):
    output_files = sorted(os.listdir(output_dir))
    gt_files = sorted(os.listdir(gt_dir))
    empty_gt_files = []
    empty_output_files = []
    percentages_missing_paras = []
    for i in range(0, len(output_files)):
        if output_files[i] == gt_files[i]:
            gt_path = gt_dir + "/" + gt_files[i]
            output_path = output_dir + "/" + output_files[i]
            if os.stat(gt_path).st_size == 0 or os.stat(output_path).st_size == 0 or size_of_file(output_path)-size_of_file(gt_path)>size_of_file(gt_path)/2:
                empty_gt_files.append(gt_files[i])
                empty_output_files.append(output_files[i])
            else:
                print("Percent of missing paragraphs for " + output_files[i] + " is " + str(
                    number_of_missing_paras(output_path, gt_path)))
                percentages_missing_paras.append(number_of_missing_paras(output_path, gt_path))
        else:
            print("Ignoring")
    if not(len(percentages_missing_paras) == 0):
        average_percentage_mis_paras = sum(percentages_missing_paras) / len(percentages_missing_paras)
    print("Number of missing paras is " + str(average_percentage_mis_paras))
    return average_percentage_mis_paras


def main(argv):
    outputfolder = ''
    groundtruthfolder = ''
    try:
        opts, args = getopt.getopt(argv, "ho:g:", ["outputfolder=", "groundtruthfolder="])
    except getopt.GetoptError:
        print('missing_paragraphs.py -o <outputfolder> -g <groundtruthfolder>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('missing_paragraphs.py -o <outputfolder> -g <groundtruthfolder>')
            sys.exit()
        elif opt in ("-o", "--outputfolder"):
            outputfolder = arg
        elif opt in ("-g", "--groundtruthfolder"):
            groundtruthfolder = arg
    evaluate_paras(outputfolder, groundtruthfolder)


if __name__ == "__main__":
    main(sys.argv[1:])
