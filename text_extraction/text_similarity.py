#!/usr/bin/python3

import sys, getopt
import os
from Levenshtein import distance


def size_of_file(file):
    contents = convert_to_string_with_paras(file)
    return len(contents)


def convert_to_string_with_paras(doc_path):
    with open(doc_path, 'r') as myfile:
        data = myfile.read()
    return data


def convert_to_string(doc_path):
    with open(doc_path, 'r') as myfile:
        data = myfile.read().replace('\n', ' ')
    return data


def score_lev_reversed(gt_file, output_file):
    gt = convert_to_string(gt_file)
    output = convert_to_string(output_file)
    score = (len(gt) - distance(gt, output)) / len(gt) * 100
    return score


def evaluate_text(output_dir, gt_dir):
    output_files = sorted(os.listdir(output_dir))
    gt_files = sorted(os.listdir(gt_dir))
    levenshteins = []
    empty_gt_files = []
    empty_output_files = []
    for i in range(0, len(output_files)):
        if output_files[i] == gt_files[i]:
            gt_path = gt_dir + "/" + gt_files[i]
            output_path = output_dir + "/" + output_files[i]
            if os.stat(gt_path).st_size == 0 or os.stat(output_path).st_size == 0 or size_of_file(output_path)-size_of_file(gt_path)>size_of_file(gt_path)/2:
                empty_gt_files.append(gt_files[i])
                empty_output_files.append(output_files[i])
            else:
                levenshteins.append(score_lev_reversed(gt_path, output_path))
                print("Processed " + output_path)
        else:
            print("Ignoring, an output file does not correspond a ground truth file.")
    # find the average levenshtein score
    if not (len(levenshteins) == 0):
        average_levenshtein = sum(levenshteins) / len(levenshteins)
    else:
        print("no scores found")
    print("The average levenshtein score for text for " + str(len(levenshteins)) + " documents is: " + str(
        average_levenshtein))
    return average_levenshtein


def main(argv):
    outputfolder = ''
    groundtruthfolder = ''
    try:
        opts, args = getopt.getopt(argv, "ho:g:", ["outputfolder=", "groundtruthfolder="])
    except getopt.GetoptError:
        print('text_similarity.py -o <outputfolder> -g <groundtruthfolder>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('text_similarity.py -o <outputfolder> -g <groundtruthfolder>')
            sys.exit()
        elif opt in ("-o", "--outputfolder"):
            outputfolder = arg
        elif opt in ("-g", "--groundtruthfolder"):
            groundtruthfolder = arg
    evaluate_text(outputfolder, groundtruthfolder)


if __name__ == "__main__":
    main(sys.argv[1:])
